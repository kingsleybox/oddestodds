# Introduction 
This project is a POC targeted at creating basic functionalities for a betting application. The core functionality is to be able to manage odds on the platform in real time, where users can view odds and backoffice can manage the odds all in real time 

# Getting Started
To test the application, you need to do the follolowing:

- Ensure you have Visual Studio installed, prefereably 2019
- Clone the code from the repository
- Open the project using visual studio by opening this file found the in the main directory
```
ODDESTODDS.sln
```
- Open Solution Explorer
- Right click the Solution and click on "Restore Nuget Packages",
this should ensure that all dependecies are downloaded successfully
- Open the Web.Config in the **ODDESTODDS** project
- Edit the ConnectionString section to point to a valid database, ensure to 
add the required credentials if needed
- Ensure the Startup project is **ODDESTODDS**
- Open "Package Manager Console", ensure the defaut project selected is 
"ODDESTODDS.Core"
run the following command on the console
```
Update-database
```
this should successfully create the database and seed the some data
- Click on Debug and Select "Start Debugging" to run the application
 
