﻿using ODDESTODDS.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.Core.DatabaseContext
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("default")
        {

        }

        public DbSet<Continent> Continents { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<GameOdd> GameOdds { get; set; }
        public DbSet<Team> Teams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Game>()
                .HasRequired(h => h.HomeTeam)
                .WithMany()
                .WillCascadeOnDelete(false);
            modelBuilder.Entity<Game>()
                .HasRequired(a => a.AwayTeam)
                .WithMany()
                .WillCascadeOnDelete(false);
        }
    }
}
