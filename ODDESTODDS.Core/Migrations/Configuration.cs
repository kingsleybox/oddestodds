﻿namespace ODDESTODDS.Core.Migrations
{
    using ODDESTODDS.Core.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ODDESTODDS.Core.DatabaseContext.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ODDESTODDS.Core.DatabaseContext.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            context.Continents.AddOrUpdate(x => x.Id,
                    new Continent(1, "Africa"),
                    new Continent(2, "North America"),
                    new Continent(3, "Europe"),
                    new Continent(4, "Asia")
                );
            context.Countries.AddOrUpdate(x => x.Id,
                new Country(1, "Nigeria", 1),
                new Country(2, "USA", 2),
                new Country(3, "Malta", 3),
                new Country(4, "Germany", 3),
                new Country(5, "Spain", 3),
                new Country(6, "England", 3)
                );
            context.Teams.AddOrUpdate(x => x.Id,
                new Team(1, "Eyimba", 1),
                new Team(2, "Hibernians F.C", 3),
                new Team(3, "Manchester United", 6),
                new Team(4, "Manchester City", 6),
                new Team(5, "Barcelona", 5),
                new Team(6, "Real Madrid", 5)
                );
        }
    }
}
