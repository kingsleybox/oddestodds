﻿using ODDESTODDS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.Core.ViewModel
{
    public class TeamViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Continent { get; set; }

        public static implicit operator TeamViewModel(Team model)
        {
            return model == null ? null : new TeamViewModel
            {
                Id = model.Id,
                Name = model.Name,
                Country = model.Country?.Name,
            };
        }
    }
}
