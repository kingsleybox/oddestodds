﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.Core.ViewModel
{
    public class CreateGameViewModel
    {
        [Required]
        public int HomeTeamId { get; set; }
        [Required]
        public int AwayTeamId { get; set; }
        [Required]
        public float HomeWinOdd { get; set; }
        [Required]
        public float AwayWinOdd { get; set; }
        [Required]
        public float Draw { get; set; }
    }

    public class EditGameViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int HomeTeamId { get; set; }
        [Required]
        public int AwayTeamId { get; set; }
        [Required]
        public float HomeWinOdd { get; set; }
        [Required]
        public float AwayWinOdd { get; set; }
        [Required]
        public float Draw { get; set; }
    }
}
