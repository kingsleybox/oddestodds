﻿using ODDESTODDS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.Core.ViewModel
{
    public class OddsViewModel
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public float Odd { get; set; }
        public string OddTitle { get; set; }

        public static implicit operator OddsViewModel(GameOdd model)
        {
            return model == null ? null : new OddsViewModel
            {
                Id = model.Id,
                GameId = model.GameId,
                Odd = model.Odd,
                OddTitle = model.OddTitle
            };
        }
    }
}
