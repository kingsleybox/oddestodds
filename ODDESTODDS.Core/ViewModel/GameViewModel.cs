﻿using ODDESTODDS.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ODDESTODDS.Core.Statics.ApplicationConstants;

namespace ODDESTODDS.Core.ViewModel
{
    public class GameViewModel
    {
        public int Id { get; set; }
        public string HomeTeam { get; set; }
        public int HomeTeamId { get; set; }
        public string AwayTeam { get; set; }
        public int AwayTeamId { get; set; }
        public List<OddsViewModel> Odds { get; set; } = new List<OddsViewModel>();

        public static implicit operator GameViewModel(Game model)
        {
            return model == null ? null : new GameViewModel
            {
                Id = model.Id,
                HomeTeam = model.HomeTeam?.Name,
                HomeTeamId = model.HomeTeamId,
                AwayTeam = model.AwayTeam?.Name,
                AwayTeamId = model.AwayTeamId,
                Odds = model.Odds.Select(x => (OddsViewModel)x).ToList()
            };
        }
    }

    public class GameModel
    {
        public int Id { get; set; }
        public string HomeTeam { get; set; }
        public int HomeTeamId { get; set; }
        public string AwayTeam { get; set; }
        public int AwayTeamId { get; set; }
        public float? HomeOdd { get; set; }
        public float? DrawOdd { get; set; }
        public float? AwayOdd { get; set; }

        public static implicit operator GameModel(Game model)
        {
            var _odds = model.Odds;
            return model == null ? null : new GameModel
            {
                Id = model.Id,
                HomeTeam = model.HomeTeam?.Name,
                HomeTeamId = model.HomeTeamId,
                AwayTeam = model.AwayTeam?.Name,
                AwayTeamId = model.AwayTeamId,
                HomeOdd = _odds.FirstOrDefault(x => x.OddTitle == HOME)?.Odd,
                AwayOdd = _odds.FirstOrDefault(x => x.OddTitle == AWAY)?.Odd,
                DrawOdd = _odds.FirstOrDefault(x => x.OddTitle == DRAW)?.Odd,
            };
        }
    }
}
