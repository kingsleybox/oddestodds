﻿using ODDESTODDS.Core.DatabaseContext;
using ODDESTODDS.Core.Models;
using ODDESTODDS.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ODDESTODDS.Core.Statics.ApplicationConstants;

namespace ODDESTODDS.Core.Service
{
    public class GameService : IGameService
    {
        private readonly ApplicationDbContext _dbContext;
        public GameService()
        {
            _dbContext = new ApplicationDbContext();
        }

        public IEnumerable<TeamViewModel> GetTeams()
        {
            var result = _dbContext.Teams.ToList();
            return result.Select(x => (TeamViewModel)x);
        }

        public async Task<IEnumerable<GameModel>> GetGames()
        {
            var result = _dbContext.Games.ToList();
            return result.Select(x => (GameModel)x);
        }
        public async Task<GameViewModel> GetGame(int id)
        {
            var game = _dbContext.Games.Find(id);
            if (game == null)
                return null;
            return game;
        }

        public async Task<int> DeleteGame(int id)
        {
            var game = _dbContext.Games.Find(id);
            if (game == null)
                return 0;
            _dbContext.Games.Remove(game);
            _dbContext.SaveChanges();
            return 1;
        }

        public async Task<int> CreateGame(CreateGameViewModel model)
        {
            try
            {
                _dbContext.Games.Add(new Game
                {
                    AwayTeamId = model.AwayTeamId,
                    HomeTeamId = model.HomeTeamId,
                    Odds = new List<GameOdd>()
                    {
                        new GameOdd(HOME, model.HomeWinOdd),
                        new GameOdd(DRAW, model.Draw),
                        new GameOdd(AWAY, model.AwayWinOdd)
                    }
                });
                _dbContext.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<int> UpdateGame(int id, CreateGameViewModel model)
        {
            try
            {
                var game = _dbContext.Games.Find(id);
                if (game == null)
                    return 0;
                game.AwayTeamId = model.AwayTeamId;
                game.HomeTeamId = model.HomeTeamId;

                var homeOdd = game.Odds.FirstOrDefault(x => x.OddTitle == HOME);
                if (homeOdd != null)
                {
                    if (homeOdd.Odd != model.HomeWinOdd)
                    {
                        homeOdd.Odd = model.HomeWinOdd;
                    }
                }
                else
                {
                    game.Odds.Add(new GameOdd(HOME, model.HomeWinOdd));
                }

                var awayOdd = game.Odds.FirstOrDefault(x => x.OddTitle == AWAY);
                if (awayOdd != null)
                {
                    if (awayOdd.Odd != model.AwayWinOdd)
                    {
                        awayOdd.Odd = model.AwayWinOdd;
                    }
                }
                else
                {
                    game.Odds.Add(new GameOdd(AWAY, model.AwayWinOdd));
                }

                var drawOdd = game.Odds.FirstOrDefault(x => x.OddTitle == DRAW);
                if (drawOdd != null)
                {
                    if (drawOdd.Odd != model.Draw)
                    {
                        drawOdd.Odd = model.Draw;
                    }
                }
                else
                {
                    game.Odds.Add(new GameOdd(DRAW, model.Draw));
                }

                _dbContext.SaveChanges();

                return 1;

            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
