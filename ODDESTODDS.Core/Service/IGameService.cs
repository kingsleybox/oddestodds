﻿using ODDESTODDS.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.Core.Service
{
    public interface IGameService
    {
        IEnumerable<TeamViewModel> GetTeams();
        Task<IEnumerable<GameModel>> GetGames();
        Task<GameViewModel> GetGame(int id);
        Task<int> DeleteGame(int id);
        Task<int> CreateGame(CreateGameViewModel model);
        Task<int> UpdateGame(int id, CreateGameViewModel model);
    }
}
