﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.Core.Models
{
    public class Team
    {
        public Team()
        {

        }

        public Team(int id, string name, int countryId)
        {
            Id = id;
            CountryId = countryId;
            Name = name;
        }
        public int Id { get; set; }
        public int CountryId { get; set; }
        public string Name { get; set; }

        public virtual Country Country { get; set; }
    }
}
