﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.Core.Models
{
    public class GameOdd
    {
        public GameOdd()
        {

        }
        public GameOdd(string title, float odd)
        {
            OddTitle = title;
            Odd = odd;
        }
        public int Id { get; set; }
        public int GameId { get; set; }
        public string OddTitle { get; set; }
        public float Odd { get; set; }
    }
}
