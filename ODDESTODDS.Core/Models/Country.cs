﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.Core.Models
{
    public class Country
    {
        public Country()
        {

        }
        public Country(int id, string name, int continentId)
        {
            Id = id;
            ContinentId = continentId;
            Name = name;
        }
        public int Id { get; set; }
        public int ContinentId { get; set; }
        public string Name { get; set; }

        public virtual Continent Continent { get; set; }
    }
}
