﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDESTODDS.Core.Statics
{
    public class ApplicationConstants
    {
        public const string HOME = "1";
        public const string AWAY = "2";
        public const string DRAW = "X";
    }
}
