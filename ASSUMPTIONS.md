1. I will not be using not be using Dependecy Injection, because this would require that I include third party libraries such as Unity etc, to achieve this,
and that would make this POC a bit more complicated and require more time.

I would have used Asp.Net Core, which supports Dependecy Injection natively, but based on the instruction "focusing on .NET technologies", 
I assumed it meant only Asp.Net Framework.

2. I also did not use the Repository Pattern in this POC, this would help abstract the database implementation from the main appliaction logic,
but to save time and complete this in the shorted possible time, I used my database context directly in my application logic

3. I did not implement any form of authentication and authorization, because I assumed that it is not part of the scope of the POC

4. I could not understand the 5th requirement, I am unsure what it means 

I want	A publish button in the backoffice
So that	I can view the latest odds on the screen