﻿$(function () {
    _connection = $.connection.gameHub;
    // Create a function that the hub can call back to display messages.

    _connection.client.getGames = function (games) {
        _games = games;
        _isHandler = true;
        ReloadingTable()
    };

    _connection.client.deletedGame = function (id) {
        RemoveDeletedGame(id);
    };

    $.connection.hub.start().done(function () {
        _connection.server.onConnect($.connection.hub.id);
    });

    var submitBtn = $("#submit-btn")[0];
    if (submitBtn != undefined) {
        submitBtn.onclick = handleFormSubmit;
    }
});

function handleFormSubmit(e) {
    e.preventDefault();
    console.log("trying to create odds");
    var form = document.getElementsByTagName("Form")[0];
    var formData = {};
    console.log(form.elements[0].name)
    for (let i = 0; i < form.elements.length; i++) {
        formData[form.elements[i].name] = form.elements[i].value
    }
    console.log(form);
    if (form.action.includes("Create")) {
        console.log("true");
        _connection.server.createGame(formData);
    }
    else {
        var pathArray = window.location.pathname.split('/');
        formData["Id"] = pathArray[pathArray.length - 1];
        _connection.server.editGame(formData);
    }
    window.location.href = "/handler/realtime"

}

function HandleEditOdds(elem) {
    window.location = "/handler/edit/" + elem.id;
}