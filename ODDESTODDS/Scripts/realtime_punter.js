﻿$(function () {
    _connection = $.connection.gameHub;
    // Create a function that the hub can call back to display messages.

    _connection.client.getGames = function (games) {
        _games = games;
        _isHandler = false;
        ReloadingTable()
    };

    _connection.client.deletedGame = function (id) {
        RemoveDeletedGame(id);
    };

    $.connection.hub.start().done(function () {
        _connection.server.onConnect($.connection.hub.id);        
    });
 });