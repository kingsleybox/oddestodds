﻿var _connection = null;
var _games = [];
var _isHandler = false;

function HandleDeleteOdds(elem) {
    _connection.server.deleteGame(elem.id);
}

function RemoveDeletedGame(id) {
    _games = _games.filter(x => x.Id != id);
    ReloadingTable();
}

function ReloadingTable() {
    var table = $('#games')[0];
    if (table != undefined) {  
        var length = table.rows.length;
        for (var i = 1; i < length; i++) {
            table.deleteRow(-1)
        }

        for (var i = 0; i < _games.length; i++) {
            var game = _games[i];
            var row = table.insertRow(1);
            var cell = row.insertCell(0);
            cell.innerHTML = game.HomeTeam;
            cell = row.insertCell(1);
            cell.innerHTML = game.AwayTeam;
            cell = row.insertCell(2);
            cell.innerHTML = game.HomeOdd;
            cell = row.insertCell(3);
            cell.innerHTML = game.DrawOdd;
            cell = row.insertCell(4);
            cell.innerHTML = game.AwayOdd

            //Add punter buttons
            if (_isHandler) {
                //var selBtnHtml = "<Button class='btn-success' data-name='" + item.name + "' id='" + item.id + "' onclick='HandleSelectOdds(this)'>Select</Button>";
                var delBtnHtml = "<Button class='btn-danger' id='" + game.Id + "' onclick='HandleDeleteOdds(this)'>Delete</Button>";
                var editBtnHtml = "<Button class='btn-info' id='" + game.Id + "' onclick='HandleEditOdds(this)' >Edit</button>"

                //cell6.innerHTML = "<span>" + delBtnHtml + "</span>" + "<span>" + editBtnHtml + "</span>"
                cell = row.insertCell(5);
                cell.innerHTML = delBtnHtml + editBtnHtml;
            }
        }
    }
}