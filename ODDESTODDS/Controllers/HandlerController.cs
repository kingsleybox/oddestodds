﻿using ODDESTODDS.Core.Service;
using ODDESTODDS.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static ODDESTODDS.Core.Statics.ApplicationConstants;

namespace ODDESTODDS.Controllers
{
    public class HandlerController : Controller
    {
        private IGameService _gameService;
        public HandlerController()
        {
            _gameService = new GameService();
        }
        // GET: Handler
        public async Task<ActionResult> Index()
        {
            var games = await _gameService.GetGames();
            return View(games.ToList());
        }

        public async Task<ActionResult> Details(int id)
        {
            var game = await _gameService.GetGame(id);
            return View(game);
        }

        public async Task<ActionResult> Create()
        {
            var teams = _gameService.GetTeams().ToList().Select(x => new SelectListItem()
            {
                Value = x.Id.ToString(),
                Text = x.Name,
            }).ToList();
            ViewBag.Teams = teams;
            return View();
        }

        public async Task<ActionResult> Edit(int Id)
        {
            var teams = _gameService.GetTeams().ToList().Select(x => new SelectListItem()
            {
                Value = x.Id.ToString(),
                Text = x.Name,
            }).ToList();
            ViewBag.Teams = teams;

            var game = await _gameService.GetGame(Id);
            if (game == null)
                return View();
            var gameModel = new CreateGameViewModel
            {
                HomeTeamId = game.HomeTeamId,
                AwayTeamId = game.AwayTeamId,
                HomeWinOdd = (float)game.Odds.FirstOrDefault(x => x.OddTitle == HOME)?.Odd,
                AwayWinOdd = (float)game.Odds.FirstOrDefault(x => x.OddTitle == AWAY)?.Odd,
                Draw = (float)game.Odds.FirstOrDefault(x => x.OddTitle == DRAW)?.Odd,
            };
            return View(gameModel);
        }


        public async Task<ActionResult> RealTime()
        {
            ViewBag.Title = "Handler Real Time View";
            var games = await _gameService.GetGames();
            return View(games.ToList());
        }
    }
}