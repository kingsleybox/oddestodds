﻿using ODDESTODDS.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ODDESTODDS.Controllers
{
    public class PunterController : Controller
    {
        private IGameService _gameService;
        public PunterController()
        {
            _gameService = new GameService();
        }
        // GET: Punter
        public async Task<ActionResult> Index()
        {
            var games = await _gameService.GetGames();
            return View(games.ToList());
        }

        public async Task<ActionResult> RealTime()
        {
            ViewBag.Title = "Punter Real Time View";
            var games = await _gameService.GetGames();
            return View(games.ToList());
        }
    }
}