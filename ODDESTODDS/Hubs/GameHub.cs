﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using ODDESTODDS.Core.Service;
using ODDESTODDS.Core.ViewModel;

namespace ODDESTODDS.Hubs
{
    public class GameHub : Hub
    {
        private readonly IGameService _gameService;

        public GameHub()
        {
            _gameService = new GameService();
        }

        public async Task OnConnect(string clientId)
        {
            var games = await _gameService.GetGames();
            Clients.Client(clientId).getGames(games);
        }

        public async Task DeleteGame(int id)
        {
            var result = await _gameService.DeleteGame(id);
            if (result != 1)
                return;
            Clients.All.deletedGame(id);
        }
        public async Task GetGames()
        {
            var games = await _gameService.GetGames();
            Clients.All.getGames(games.ToList());
        }
        public async Task CreateGame(CreateGameViewModel model)
        {
            var result = await _gameService.CreateGame(model);
            if (result != 1)
                return;
            await GetGames();
        }
        public async Task EditGame(EditGameViewModel model)
        {
            var gameModel = new CreateGameViewModel
            {
                HomeTeamId = model.HomeTeamId,
                AwayTeamId = model.AwayTeamId,
                AwayWinOdd = model.AwayWinOdd,
                HomeWinOdd = model.HomeWinOdd,
                Draw = model.Draw,
            };
            var result = await _gameService.UpdateGame(model.Id, gameModel);
            if (result != 1)
                return;
            await GetGames();
        }
    }
}